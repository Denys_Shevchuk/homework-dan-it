    class Modal {
        constructor() {
            this.card = 'https://ajax.test-danit.com/api/cards';
            this.login = 'https://ajax.test-danit.com/api/cards/login';
            this.createCardBtn = document.querySelector('.create_card_btn');
            this.modalBg = document.querySelector('.modal-bg');
            this.pageNoItemMessage  = document.createElement('p');
            this.popupBtn = document.querySelector('.modal-btn');
            this.searchForm = document.querySelector('.second_block');
            this.cardPlace = document.querySelector('.place_for_cards');
            this.cardPlace.classList.remove('hidden');
        }

        getCardInfo() {
            return fetch(this.card,{
                method: 'GET',
                headers: {
                    Authorization: `Bearer ${'8d17ad32-77c0-4390-8bb7-c01b7d832bf2'}`,
                    'Content-Type': 'application/json',
                },
            }).then(res => console.log(res.json()))
        }
        getLogin() {

            return fetch(this.login, {
                method: 'POST',
                body: JSON.stringify({
                    // email: "program.creator@gmail.com",
                    // password: "hardPa$sword",
                    email:  document.getElementById("email").value,
                    password: document.getElementById("password").value,
                }),
                headers: {
                    'Content-Type': 'application/json',
                },

            })
                .then(resp => resp.text())
                .then(data => {

                    if (data  === "8d17ad32-77c0-4390-8bb7-c01b7d832bf2") {
                        sessionStorage.setItem('token', data);
                        this.signInTransition();
                        this.createCardBtn.classList.remove('hidden');
                        this.modalBg.classList.remove('bg-active');
                        this.pageNoItemMessage.textContent = '';
                        this.popupBtn.style.cssText = 'display: none;';
                        this.searchForm.classList.remove('hidden');
                        this.cardPlace.style.cssText = 'display: flex';
                        this.cardPlace.classList.remove('hidden');
                        let token = sessionStorage.getItem( 'token');
                        console.log(token);
                    } else {
                        alert('Введен неверный логин или пароль')
                    }
                })
        }

        createPopup() {
            const popupBtn           = document.querySelector('.enter-btn');
            const pageNoItemMessage  = document.createElement('p');
            const modalBg            = document.querySelector('.modal-bg');
            const modalClose         = document.querySelector('.modal-close');

            pageNoItemMessage.textContent = 'Please LogIn.';
            pageNoItemMessage.style.cssText = 'font-weight: bold; font-size: 40px; text-align: center;';
            document.body.append(pageNoItemMessage);

            popupBtn.addEventListener('click', () => {
                modalBg.classList.add('bg-active');
                pageNoItemMessage.textContent = '';
            });

            modalClose.addEventListener('click', () => {
                modalBg.classList.remove('bg-active');
                pageNoItemMessage.textContent = '';
            });
        }

        signInTransition() {

            const signInBtn = document.querySelector('.sign_in_btn');
            signInBtn.addEventListener('click', (e) => {
                modal.getLogin();
                e.preventDefault();
            });
        }

    }

    const modal = new Modal();
    modal.createPopup();
    modal.signInTransition();

    class Form {
        constructor() {
        }
        createCard() {
            const createNewCardBtn = document.querySelector('.create_card_btn');
            const template = document.querySelector('.my-template');
            const select = document.querySelector('.select-doctor');
            const therapist = document.querySelector('.therapist');
            const cardiologist = document.querySelector('.cardiologist');
            const dentist = document.querySelector('.dentist');
            const card = document.querySelector('.card');
            dentist.classList.add('hidden');
            therapist.classList.add('hidden');
            cardiologist.classList.add('hidden');

            createNewCardBtn.addEventListener('click', () => {
                template.classList.remove('hidden');
            });
            select.addEventListener('click', function() {
                if (this.value === 'Стоматолог')
                {
                    dentist.classList.remove('hidden');
                    card.style.height = '700px';
                    therapist.classList.add('hidden');
                    cardiologist.classList.add('hidden');
                }
                else if (this.value === 'Терапевт')
                {
                    therapist.classList.remove('hidden');
                    dentist.classList.add('hidden');
                    cardiologist.classList.add('hidden');
                    card.style.height = '600px';
                }
                else if (this.value === 'Кардиолог') {
                    dentist.classList.add('hidden');
                    therapist.classList.add('hidden');
                    cardiologist.classList.remove('hidden');

                    card.style.height = '1040px';
                }
            });
        }
        postCardInfo() {
            const template = document.querySelector('.my-template');
            const therapist = document.querySelector('.therapist');
            const cardiologist = document.querySelector('.cardiologist');
            const dentist = document.querySelector('.dentist');
            const cardVisit = document.getElementById('cardiologist-purpose-visit');
            const visitDesc = document.getElementById('cardiologist-visit-description');
            const normalPressure = document.getElementById('normal-pressure');
            const bodyMass = document.getElementById('body-mass-index');
            const cardiovascularSystem = document.getElementById('cardiovascular-system');
            const age = document.getElementById('age');
            const fullName = document.getElementById('cardiologist-full-name');
            const submitBtn = document.querySelector('.submit-btn');
            const canselBtn = document.querySelector('.cancel-btn');
            const appBlock = document.querySelector('.appointment-block');

            // Dentist
            const dentistPurposeVisit = document.getElementById('dentist-purpose-visit');
            const dentistDescVisit = document.getElementById('dentist-visit-description');
            // const chooseSpeed
            const dentistLastVisit = document.getElementById('dentist-last-visit');
            const dentistFullName = document.getElementById('dentist-full-name');

            // Therapist
            const therapistPurposeVisit = document.getElementById('therapist-purpose-visit');
            const therapistVisitDescription = document.getElementById('therapist-visit-description');
            // const chooseSpeed
            const therapistVisitUrgency = document.getElementById('therapist-choose-speed');
            const therapistFullName = document.getElementById('therapist-full-name');
            // if (!cardiologist) {
            //     submitBtn.addEventListener('click', () => {
            //         fetch(`https://ajax.test-danit.com/api/cards/`, {
            //             method: 'POST',
            //             body: JSON.stringify({
            //                 visitPurpose: cardVisit.value,
            //                 visitDescription: visitDesc.value,
            //                 pressure: normalPressure.value,
            //                 yourMass: bodyMass.value,
            //                 bloodSystem: cardiovascularSystem.value,
            //                 yourAge: age.value,
            //                 surname: fullName.value,
            //             }),
            //             headers: {
            //                 Authorization: `Bearer ${'8d17ad32-77c0-4390-8bb7-c01b7d832bf2'}`,
            //                 'Content-Type': 'application/json',
            //             }
            //         }).then(r => {
            //             console.log(r)
            //         });
            //     });
            // }
            // if (cardiologist) {
            //     submitBtn.addEventListener('click', () => {
            //         fetch(`https://ajax.test-danit.com/api/cards/`, {
            //             method: 'POST',
            //             body: JSON.stringify({
            //                 visitPurpose: cardVisit.value,
            //                 visitDescription: visitDesc.value,
            //                 pressure: normalPressure.value,
            //                 yourMass: bodyMass.value,
            //                 bloodSystem: cardiovascularSystem.value,
            //                 yourAge: age.value,
            //                 surname: fullName.value,
            //
            //                 // DentistVisitPurpose: dentistPurposeVisit.value,
            //                 // DentistVisitDescription: dentistDescVisit.value,
            //                 // dentistLastVisit: dentistLastVisit.value,
            //                 // dentistFullName: dentistFullName.value,
            //             }),
            //             headers: {
            //                 Authorization: `Bearer ${'8d17ad32-77c0-4390-8bb7-c01b7d832bf2'}`,
            //                 'Content-Type': 'application/json',
            //             }
            //         }).then(res => {
            //                 return res.json();
            //
            //             }
            //         ).then(data => {
            //             const card = document.querySelector('.card');
            //             const cardBg = document.querySelector('.appointment-block');
            //             const paragraph = document.createElement('p');
            //             paragraph.innerHTML = `
            //                 <div style="border: 6px solid #a600a6;
            //                             border-radius: 5px;
            //                             padding: 15px;
            //                             width: 300px;
            //                             height: 150px;
            //                             background: #755881;">
            //                             <p style="text-align: center;
            //                                       color: #faebff;
            //                                       font-weight: bold;
            //                                       margin-bottom: 5px;
            //                                       font-family: 'Monserat', sans-serif;">Карточка</p>
            //                     <div style="color: #fff;
            //                      border: 2px solid #bfd9ff;
            //                      display: inline-block;
            //                      border-radius: 3px;
            //                      margin-bottom: 5px;
            //                      padding: 5px;">TherapistFullName: ${data.content.dentistFullName}</div>
            //                 </div>
            //             `;
            //             template.classList.add('hidden');
            //             cardBg.style.backgroundColor = 'rgba(0, 0, 0, 0)';
            //         })
            //
            //     });
            // }
            // canselBtn.addEventListener('click', () => {
            //     template.classList.add('hidden')
            // });


            if (dentist) {
                submitBtn.addEventListener('click', () => {
                    fetch(`https://ajax.test-danit.com/api/cards/`, {
                        method: 'POST',
                        body: JSON.stringify({

                            DentistVisitPurpose: dentistPurposeVisit.value,
                            DentistVisitDescription: dentistDescVisit.value,
                            dentistLastVisit: dentistLastVisit.value,
                            dentistFullName: dentistFullName.value,

                        }),
                        headers: {
                            Authorization: `Bearer ${'8d17ad32-77c0-4390-8bb7-c01b7d832bf2'}`,
                            'Content-Type': 'application/json',
                        }
                    }).then(res => {
                            return res.json();

                        }
                    ).then(data => {
                        const card = document.querySelector('.card');
                        const cardBg = document.querySelector('.appointment-block');
                        const paragraph = document.createElement('p');
                        paragraph.innerHTML = `
                            <div style="border: 6px solid #a600a6;
                                        border-radius: 5px;
                                        padding: 15px;
                                        width: 300px;
                                        height: 150px;
                                        background: #755881;">
                                        <p style="text-align: center;
                                                  color: #faebff;
                                                  font-weight: bold;
                                                  margin-bottom: 5px;
                                                  font-family: 'Monserat', sans-serif;">Карточка</p>
                                <div style="color: #fff;
                                 border: 2px solid #bfd9ff;
                                 display: inline-block;
                                 border-radius: 3px;
                                 margin-bottom: 5px;
                                 padding: 5px;
                                ">TherapistVisitPurpose: ${data.content.DentistVisitPurpose}}</div>
                                <div style="color: #fff;
                                 border: 2px solid #bfd9ff;
                                 display: inline-block;
                                 border-radius: 3px;
                                 margin-bottom: 5px;
                                 padding: 5px;">TherapistVisitDescription: ${data.content.DentistVisitDescription}</div>
                                  <div style="color: #fff;
                                 border: 2px solid #bfd9ff;
                                 display: inline-block;
                                 border-radius: 3px;
                                 margin-bottom: 5px;
                                 padding: 5px;">dentistLastVisit: ${data.content.dentistLastVisit}</div>
                                <div style="color: #fff;
                                 border: 2px solid #bfd9ff;
                                 display: inline-block;
                                 border-radius: 3px;
                                 margin-bottom: 5px;
                                 padding: 5px;">TherapistFullName: ${data.content.dentistFullName}</div>
                            </div>
                        `;
                        template.classList.add('hidden');
                        cardBg.style.backgroundColor = 'rgba(0, 0, 0, 0)';
                    })

                });
            }
            canselBtn.addEventListener('click', () => {
                template.classList.add('hidden')
            });


            // if (therapist)
            // {
            //     submitBtn.addEventListener('click', () => {
            //         fetch(`https://ajax.test-danit.com/api/cards/`, {
            //             method: 'POST',
            //             body: JSON.stringify({
            //                 TherapistVisitPurpose:      therapistPurposeVisit.value,
            //                 TherapistVisitDescription:  therapistVisitDescription.value,
            //                 TherapistVisitUrgency:      therapistVisitUrgency.value,
            //                 TherapistFullName:          therapistFullName.value,
            //
            //             }),
            //             headers: {
            //                 Authorization: `Bearer ${'8d17ad32-77c0-4390-8bb7-c01b7d832bf2'}`,
            //                 'Content-Type': 'application/json',
            //             }
            //         }) .then(res => {
            //             return res.json();
            //         }
            //     ).then(data => {
            //             const card = document.querySelector('.card');
            //             const cardBg = document.querySelector('.appointment-block');
            //             const paragraph = document.createElement('p');
            //             paragraph.innerHTML = `
            //                 <div style="border: 6px solid #a600a6;
            //                             border-radius: 5px;
            //                             padding: 15px;
            //                             width: 300px;
            //                             height: 150px;
            //                             background: #755881;">
            //                             <p style="text-align: center;
            //                                       color: #faebff;
            //                                       font-weight: bold;
            //                                       margin-bottom: 5px;
            //                                       font-family: 'Monserat', sans-serif;">Карточка</p>
            //                     <div style="color: #fff;
            //                      border: 2px solid #bfd9ff;
            //                      display: inline-block;
            //                      border-radius: 3px;
            //                      margin-bottom: 5px;
            //                      padding: 5px;
            //                     ">TherapistVisitPurpose: ${data.content.TherapistVisitPurpose}</div>
            //                     <div style="color: #fff;
            //                      border: 2px solid #bfd9ff;
            //                      display: inline-block;
            //                      border-radius: 3px;
            //                      margin-bottom: 5px;
            //                      padding: 5px;">TherapistVisitDescription: ${data.content.TherapistVisitDescription}</div>
            //                     <div style="color: #fff;
            //                      border: 2px solid #bfd9ff;
            //                      display: inline-block;
            //                      border-radius: 3px;
            //                      margin-bottom: 5px;
            //                      padding: 5px;">TherapistFullName: ${data.content.TherapistFullName}</div>
            //                 </div>
            //             `;
            //             template.classList.add('hidden')
            //             cardBg.style.backgroundColor = 'rgba(0, 0, 0, 0)';
            //         })
            //     });
            // }
            canselBtn.addEventListener('click', () =>{
                template.classList.add('hidden')
            })
        }
         getCardInfo() {
                return fetch(`https://ajax.test-danit.com/api/cards/`,{
                    method: 'GET',
                    headers: {
                        Authorization: `Bearer ${'8d17ad32-77c0-4390-8bb7-c01b7d832bf2'}`,
                        'Content-Type': 'application/json',
                    }
                }
             )
         }
    }

    const form = new Form();
    const place = document.querySelector(".card_bg");

    console.log(form.createCard());
    console.log(form.postCardInfo());
    console.log(form.getCardInfo().then(res => {

            return res.json().then(obj =>
                obj.forEach(dat => {
                    console.log(dat);
                    const therapist     = document.querySelector('.therapist');

                    const cardiologist  = document.querySelector('.cardiologist');

                    const dentist       = document.querySelector('.dentist');
                    const id            = dat.content.id;

                    if(!therapist) {
                        const paragraphTer = document.createElement('div');
                        paragraphTer.innerHTML =
                             `
                                <p>Цель визита: ${dat.content.TherapistVisitPurpose}</p>
                                <p>Краткое описание: ${dat.content.TherapistVisitDescription}</p>
                                <p>Выберете срочность: ${dat.content.TherapistVisitUrgency}</p>
                                <p>ФИО: ${dat.content.TherapistFullName}</p>

                                <button data-edit="${dat.id}" class="edit-btn">edit</button>
                                <button data-delete="${dat.id}" class="del-btn">delete</button>
                             `;
                        place.prepend(paragraphTer);
                    }

                    if(dentist) {
                        console.log(dentist);
                        const paragraphTer = document.createElement('div');
                        paragraphTer.innerHTML =
                            `
                                <p>Цель визита: ${dat.content.DentistVisitPurpose}</p>
                                <p>Краткое описание: ${dat.content.DentistVisitDescription}</p>
                                <p>Дата последнего посещения: ${dat.content.dentistLastVisit}</p>
                                <p>ФИО: ${dat.content.dentistFullName}</p>
                                

                                <button data-delete="" class="del-btn">edit</button>
                                <button data-delete="${dat.id}" class="del-btn">delete</button>
                             `;
                        place.prepend(paragraphTer);
                    }
                    // if(cardiologist) {
                    //     const paragraphTer = document.createElement('div');
                    //     paragraphTer.innerHTML =
                    //         `
                    //             <p>Цель визита: ${dat.content.DentistVisitPurpose}</p>
                    //             <p>Краткое описание: ${dat.content.DentistVisitDescription}</p>
                    //             <p>ФИО: ${dat.content.dentistFullName}</p>
                    //
                    //             <button data-delete="" class="del-btn">edit</button>
                    //             <button data-delete="${dat.id}" class="del-btn">delete</button>
                    //          `;
                    //     place.prepend(paragraphTer);
                    // }
                })
            );
        })
    );



    class Delete {
        constructor(){
        }
        deleteBtn() {
            const deleteBtn = document.querySelector(".card_bg") ;

            deleteBtn.addEventListener('click',  (e) => {
                if(e.target.dataset.delete) {
                    deleteNoteFromServer(e.target.dataset.delete);
                     e.target.closest('div').remove();
                }
                });
            async function deleteNoteFromServer(id) {
                console.log(id);
                await fetch(`https://ajax.test-danit.com/api/cards/${id}`, {
                    method: 'DELETE',
                    headers: {
                        Authorization: `Bearer ${'8d17ad32-77c0-4390-8bb7-c01b7d832bf2'}`,
                        'Content-Type': 'application/json',
                    }
                });
            }
        }
        editBtn() {
            async function editCard() {
                const editBtn = document.querySelector('.edit-btn');
                editBtn.addEventListener('click', () => {
                    console.log(editBtn)
                })
                await fetch(`https://ajax.test-danit.com/api/cards/`, {
                    method: 'PUT',
                    headers: {
                        Authorization: `Bearer ${'8d17ad32-77c0-4390-8bb7-c01b7d832bf2'}`,
                        'Content-Type': 'application/json',
                    },
                })
            }
        }
    }
    // class Edit {
    //     constructor() {
    //
    //     }
    //     editBtn() {
    //         const editBtn = document.querySelector('.edit-btn');
    //         console.log(editBtn)
    //          fetch(`https://ajax.test-danit.com/api/cards/`,{
    //             method: 'PUT',
    //             headers: {
    //                 Authorization: `Bearer ${'0640d8ac-4509-443c-a8a6-872b331cde67'}`,
    //                 'Content-Type': 'application/json',
    //             },
    //             // body: {
    //             //     title: "Визит к кардиологу",
    //             //     description: 'Новое описание визита',
    //             //     doctor: "Cardiologist",
    //             //     bp: "24",
    //             //     age: 23,
    //             //     weight: 70
    //             //
    //             // }
    //         })
    //     }
    // }

    const deleteCard = new Delete();
    deleteCard.deleteBtn();
    deleteCard.editBtn();

    // const editCard = new Edit();
    // editCard.editBtn();