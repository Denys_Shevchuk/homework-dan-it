const wrapper = document.querySelector('.wrapper');
const ipBtn = document.querySelector('#ipBtn');
const self = document.createElement('div');
const continent = document.createElement('p');
const country = document.createElement('p');
const regionName = document.createElement('p');
const city = document.createElement('p');
const district = document.createElement('p');
ipBtn.addEventListener('click', onclickBtn);

async function onclickBtn() {
    const ip = await fetch('https://api.ipify.org/?format=json');
    const notes = await ip.json();

    console.log(notes);
    const info = await fetch(`http://ip-api.com/json/${notes.ip}?fields=continent,country,regionName,city,district`);
    const res = await info.json();

    continent.textContent = "continent: " + res.continent;
    country.textContent = "country: " + res.country;
    regionName.textContent = "region: " + res.regionName;
    city.textContent = "city: " + res.city;
    district.textContent = "district: " + res.district;
    self.style.border = "1px solid black";
    self.style.backgroundColor = "turquoise";
    self.style.marginTop = "40px";
    self.style.borderRadius = "10%";
    self.style.textAlign = "center";
    self.style.width = "20%";
    self.append(
        continent,
        country,
        regionName,
        city,
        district
    );
    wrapper.append(self)
}