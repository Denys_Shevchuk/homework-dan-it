const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

const div = document.querySelector("#root");
const checkList = function (list) {
    let checkedList = [];
    for (let i = 0; i < list.length; i++) {
        try {
            if (!list[i].author) {
                throw `Missing data for Author in object #${i + 1}`;
            } else if (!list[i].name) {
                throw `Missing data for book name in object #${i + 1}`;
            } else if (!list[i].price) {
                throw `Missing data for price in object #${i + 1}`;
            } else {
                checkedList.push(list[i]);
            }
        } catch (e) {
            console.error(e);
        }
    }
    return checkedList;
};
const createBookList = function (list) {
    let listItems = "";
    for (let i = 0; i < list.length; i++) {
        listItems += ` <ul>Author: ${list[i].author}, Name: ${list[i].name}, Price: ${list[i].price},</ul>`;
    }
    return listItems;
};
const chekedList = checkList(books);
const bookList = createBookList(chekedList);
div.innerHTML = bookList;


