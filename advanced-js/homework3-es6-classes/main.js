class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    get employeeName() {
        return `${this.name}`;
    }

    set employeeName(value) {
        this.name = value;
    }

    get employeeAge() {
        return `${this.age}`;
    }

    set employeeAge(value) {
        this.age = value;
    }

    get employeeSalary() {
        return `${this.salary}`;
    }

    set employeeSalary(value) {
        this.salary = value;
    }
}
class Programmer  extends Employee {
    constructor(name, age, salary, lang) {
        super(name,age,salary);
        this.lang = lang;
    }
    get programmerSalary () {
        super.employeeSalary;
        return (this.salary *3)
    }

}


// const employeeNew = new Employee('Jon', '35', '10000');
// console.log(employeeNew);



const programmerA =  new Programmer('Den', '50', '30000'  , 'USA');
const programmerB =  new Programmer('Li', '40', '90000', 'UA');
const programmerC =  new Programmer('Nikola', '30', '180000', 'CH');
console.log(programmerA);
console.log(programmerB);
console.log(programmerC);
