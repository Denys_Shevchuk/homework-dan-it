const filmURL = 'https://swapi.dev/api/films/';

class FilmStarWars {
    constructor({episode_id, title, opening_crawl, characters}) {
        this.episode = episode_id;
        this.name = title;
        this.content = opening_crawl;
        this.characters = characters;
    }
}

fetch(filmURL)
    .then(response => response.json())
    .then(films => {
        const {results} = films;
        const film = results.map(item => new FilmStarWars(item));
        const filmContainer = document.createElement('div');
        const newFilm = film.forEach(item => {
            const filmWrapper = document.createElement('div');
            const filmName = document.createElement('h3');
            const filmCharacters = document.createElement('h4');
            const filmEpisode = document.createElement('p');
            const filmContent = document.createElement('p');
            filmName.textContent = item.name;
            filmEpisode.textContent = "Episode: " + item.episode;
            filmContent.textContent = item.content;

            item.characters.forEach(url => {
                fetch(url)
                    .then(response => response.json())
                    .then(nameActor => {
                        filmCharacters.textContent +=  nameActor.name + "; ";
                    });

            });

            filmWrapper.append(
                filmName,
                filmCharacters,
                filmEpisode,
                filmContent,
            );

            filmContainer.append(filmWrapper)
        });

        document.body.prepend(filmContainer)
    });
