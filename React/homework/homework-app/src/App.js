import './App.scss';
import React from "react";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import AppRoutes from "./components/AppRoutes/AppRoutes";




const App = () => {

    return (
        <>
            <Header/>
            <AppRoutes/>
            <Footer/>
        </>
    );
};

export default App;
