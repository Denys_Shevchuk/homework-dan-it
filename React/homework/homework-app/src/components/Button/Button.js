import React from 'react';
import PropTypes from 'prop-types';
import Modal from "../Modal/Modal";

const Button  = (props) => {



        const {text, backgroundColor, onClick, buttonClass} = props;

        return (
            <>
                <button className={buttonClass} style={{backgroundColor: backgroundColor}}
                        onClick={onClick}>{text}</button>
            </>
        );

    };


Modal.propTypes = {
    name: PropTypes.string,
    price: PropTypes.string,
    src: PropTypes.string,
    article: PropTypes.string,
    color: PropTypes.string,
    onClick: PropTypes.func,
    closeButton: PropTypes.bool,
    text: PropTypes.string,
    backgroundColor: PropTypes.string,
    action: PropTypes.object,
    header: PropTypes.string,
    buttonClass: PropTypes.string

};

export default Button;