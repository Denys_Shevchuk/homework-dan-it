import React, {useEffect, useState} from 'react';
import {Route, Switch} from "react-router-dom";
import ListItems from "../ListItems/ListItems";
// import Favorites from "../ Favorites/Favorites";
// import Basket from "../ Basket/Basket";



const AppRoutes = (props) => {

    const [snowboard, setSnowboard] = useState([]);

    useEffect(() => {
        fetch('products.json', {
            headers: {
                "Content-Type": 'application/json'
            }
        })
            .then(r => {
                console.log('response', r);
                return r.json()
            })
            .then(data => {
                console.log(data);
                setSnowboard(data.snowboard);
            });
        return () => {
            console.log('App will unmount');
        }
    }, []);


    useEffect(() => {
        if (localStorage.getItem('') === null) {
            localStorage.setItem('Cart', JSON.stringify([],));
            localStorage.setItem('Favorites', JSON.stringify([]));
        }

    }, []);


    return (
        <>
            <Switch>
                <Route exact path="/favorites" component={ListItems}/>

                <Route exact path="/basket"  component={ListItems}/>

                <Route exact path="/" render={(props) =>
                    <ListItems items={snowboard}

                    />}/>

                <Route exact path="/Home" render={(props) =>
                    <ListItems items={snowboard}/>}/>

            </Switch>
        </>
    );

};

export default AppRoutes;