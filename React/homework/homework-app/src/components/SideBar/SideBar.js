import React from 'react';
import {NavLink} from "react-router-dom";


const SideBar = (props) => {

    const {items} = props;
    const sideBarItems = items.map((singleItem, index) => (
        <li key={index} className="emails-sidebar__item">
            <NavLink activeClassName='emails-sidebar__item--active'
                     to={`/${singleItem.toLowerCase()}`}>{singleItem.toUpperCase()}</NavLink>
        </li>
    ));

    return (
        <div className='emails-sidebar__div'>
            <aside>
                <ul className='emails-sidebar__ul'>
                    {sideBarItems}
                </ul>
            </aside>

        </div>
    );

};

export default SideBar;