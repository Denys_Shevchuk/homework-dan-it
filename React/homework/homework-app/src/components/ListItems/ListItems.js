import React, {useEffect, useState} from 'react';
import Product from "../Product/Product";
import PropTypes from 'prop-types';


const ListItems = (props) => {

    const {items = []} = props;
    const cards = items.map((el, index) => <Product
        card={el}
        key={index}

    />);

    const [snowboardCart, setSnowboardCart] = useState([]);
    const [snowboardFavourite, setSnowboardFavourite] = useState([]);


    useEffect(() => {

        if (snowboardFavourite) {

            const raw = localStorage.getItem('Favorites') ||
                [];
            setSnowboardFavourite(JSON.parse(raw));

        }

    }, [snowboardFavourite]);





    useEffect(() => {
        if(snowboardCart) {
            const raw = localStorage.getItem('Cart') ||
                [];
            setSnowboardCart(JSON.parse(raw));
        }
    }, [snowboardCart]);

    return (
        <div className="ListItems">
            {cards}


            {snowboardFavourite.map(index => <Product card={index} key={index.id}>{index}</Product>)}

            {snowboardCart.map(index => <Product  card={index} key={index.id}>{index}</Product>)}

        </div>

    );


};

ListItems.propTypes = {
    card: PropTypes.array,
    onClick: PropTypes.func
};

export default ListItems;