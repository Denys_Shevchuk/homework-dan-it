import React from 'react';
import PropTypes from 'prop-types';


const Modal = (props) => {

    const {header, closeButton, text, onClick, backgroundColor, action} = props;

    const wrapperClose = (e) => {
        if (e.target.getAttribute('class') === "modalOverlay") {
            onClick();
        }
    };


    return (
        <>
            <div onClick={wrapperClose} className="modalOverlay">

                <div className="modalOverlay__modal" style={{backgroundColor: backgroundColor}}>

                    <div className="modalOverlay__header">
                        {header}
                        {closeButton &&
                        <button onClick={onClick} className={'modalOverlay__btn-close-open'}>X</button>}
                    </div>

                    <div className="modalOverlay__main-content">
                        <p>{text}</p>
                    </div>
                    {action}
                </div>
            </div>
        </>
    );


}

Modal.propTypes = {
    name: PropTypes.string,
    price: PropTypes.string,
    src: PropTypes.string,
    article: PropTypes.string,
    color: PropTypes.string,
    onClick: PropTypes.func,
    closeButton: PropTypes.bool,
    text: PropTypes.string,
    backgroundColor: PropTypes.string,
    action: PropTypes.string,
    header: PropTypes.string,

};


export default Modal;


