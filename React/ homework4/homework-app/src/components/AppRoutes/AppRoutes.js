import React, {useEffect, useState} from 'react';
import {Route, Switch} from "react-router-dom";
import ListItems from "../ListItems/ListItems";
import Product from "../Product/Product";
// import Favorites from "../ Favorites/Favorites";
// import Basket from "../ Basket/Basket";



const AppRoutes = (props) => {

    const [snowboard, setSnowboard] = useState([]);
    const [snowboardFavourite, setSnowboardFavourite] = useState([]);
    const [snowboardCart, setSnowboardCart] = useState([]);

    useEffect(() => {
        fetch('products.json', {
            headers: {
                "Content-Type": 'application/json'
            }
        })
            .then(r => {
                console.log('response', r);
                return r.json()
            })
            .then(data => {
                console.log(data);
                setSnowboard(data.snowboard);
            });
        return () => {
            console.log('App will unmount');
        }
    }, []);


    useEffect(() => {
        if (localStorage.getItem('') === true ) {
            localStorage.setItem('Cart', JSON.stringify([],));
            localStorage.setItem('Favorites', JSON.stringify([]));
        }

    }, []);



    useEffect(() => {

        if (snowboardFavourite) {

            const raw = localStorage.getItem('Favorites') ||
                [];
            setSnowboardFavourite(JSON.parse(raw));

        }

    }, [snowboardFavourite]);


    useEffect(() => {
        if (snowboardCart) {
            const raw = localStorage.getItem('Cart') ||
                [];
            setSnowboardCart(JSON.parse(raw));
        }
    }, [snowboardCart]);

    return (
        <>
            <Switch>
                <Route exact path="/favorites"    render={(props) =>

                    <ListItems items={snowboardFavourite}

                    />}/>

                <Route exact path="/basket"  render={(props) =>


                    <ListItems items={snowboardCart}

                    />}/>

                <Route exact path="/"

                       render={(props) =>


                    <ListItems items={snowboard}

                    />}/>

                <Route exact path="/Home" render={(props) =>
                    <ListItems items={snowboard}/>}/>

            </Switch>
        </>
    );

};

export default AppRoutes;