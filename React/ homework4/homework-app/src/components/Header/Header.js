import React, {useState} from 'react';
import SideBar from "../SideBar/SideBar";
import {Route, Switch} from "react-router-dom";
import ListItems from "../ListItems/ListItems";

const Header = () => {

    return (

        <header>
            <div className="header">

                <div className="logo">
                    <a href="/"> <img className="img"
                                     src="https://boomerang-boardshop.ua/wp-content/uploads/2016/05/logo_w_s.png"
                                     alt="Logo"/> </a>
                </div>
                <div className="header__title_container">
                    <h2 className="header__main_title">
                        <SideBar items={ ['Home', 'Favorites', 'Basket']} />
                    </h2>
                    <div id="easynetshop-cart">
                        <div id="enscart_my_wrapper">
                            <div id="enscart_myimage_wrapper">
                                <img src="https://i.ibb.co/ZfJ03Ns/shop1.png " alt='logo'/>
                            </div>
                            <div id="enscart_my_counter_wrapper"><span id="easynetshop-cart-count"> 0 </span></div>
                        </div>
                    </div>
                </div>

            </div>
            <div className='background__img'>
                <h2 className='background__img-text'>Сноуборды</h2>
            </div>
        </header>

    );

};

export default Header;