import React, {useEffect, useState} from 'react';
import Product from "../Product/Product";
import PropTypes from 'prop-types';
import {Route} from "react-router-dom";


const ListItems = (props) => {

    const {items = []} = props;
    const cards = items.map((el, index) => <Product card={el} key={index}/>);

    return (
        <>
            <div className="ListItems">
                {cards}
            </div>
        </>
    );


};

ListItems.propTypes = {
    card: PropTypes.array,
    onClick: PropTypes.func
};

export default ListItems;