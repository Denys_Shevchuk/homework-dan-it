import React, {useEffect, useState,} from 'react';
import Button from "../Button/Button";
import PropTypes from 'prop-types';
import Modal from "../Modal/Modal";
import {Switch, Route} from "react-router-dom";
import {star} from "../../ images/star";


const Product = (props) => {

    const [isModalOpen, setIsModalOpen] = useState(false);
    const [snowboardCart, setSnowboardCart] = useState([]);
    const [snowboardFavourite = [], setSnowboardFavourite] = useState([]);
    const [starShowFilled, setStarShowFilled] = useState(false);

    const {card: {name, price, picture, code, colour, id}} = props;

    const onBtnClick = () => setIsModalOpen(true);
    const closeModals = () => setIsModalOpen(false);


    const closeModal = () => {
        setIsModalOpen(false);
        const index = id;
        setSnowboardCart(snowboardCart => [...snowboardCart, {
            index, id, name, price, picture, code, colour,
        }]);
        localStorage.setItem('Cart', JSON.stringify([...snowboardCart, {
            index, id, name, price, picture, code, colour,
        }]));

    };

    const deleteById = () => {
        localStorage.removeItem('Cart');
        setIsModalOpen(false);
        const index = id;
        setSnowboardCart([...snowboardCart.filter(e => e.id !== index)]);
        localStorage.setItem('Cart', JSON.stringify([...snowboardCart.filter(e => e.id !== index)]));
    };


    useEffect(() => {
        const raw = localStorage.getItem('Cart') ||
            [];
        setSnowboardCart(JSON.parse(raw))
    }, [isModalOpen]);


    const closeFavorite = () => {
        if (!starShowFilled) {
            console.log(!starShowFilled);
            setStarShowFilled(true);
            const index = id;
            setSnowboardFavourite(snowboardFavourite => [...snowboardFavourite, {
                index, id, name, price, picture, code, colour,
            }]);

            localStorage.setItem('Favorites', JSON.stringify([...snowboardFavourite, {
                    index, id, name, price, picture, code, colour,
                }]
                )
            )

        } else {
            setStarShowFilled(false);
            const index = id;
            localStorage.removeItem('Favorites');
            setSnowboardFavourite([...snowboardFavourite.filter(e => e.id !== index)]);
            localStorage.setItem('Favorites', JSON.stringify([...snowboardFavourite.filter(e => e.id !== index)]));
        }
    };
    const closeFavorit = () => {
        const index = id;
        localStorage.removeItem('Favorites');
        setSnowboardFavourite([...snowboardFavourite.filter(e => e.id !== index)]);
        localStorage.setItem('Favorites', JSON.stringify([...snowboardFavourite.filter(e => e.id !== index)]));
    };

    useEffect(() => {
        const fav = localStorage.getItem('Favorites') ||
            [];
        setSnowboardFavourite(JSON.parse(fav))

    }, [snowboardFavourite]);





    return (
        <>
            <Switch>
                <Route path="/" exact>
                    <div className="products">
                        <div className="jo">
                            <div className="parallelogram">
                                <div onClick={closeFavorite}>
                                    {star(starShowFilled)}
                                </div>
                            </div>
                            <img className="Avatar" src={picture} alt='snowboard'/>
                            <h5>{name}</h5>
                            <div><b>Цена </b>{price}</div>
                            <div><b>Код товара </b>{code}</div>
                            <div><b>Цвет </b>{colour}</div>

                            <Button buttonClass="button" backgroundColor={"black"}
                                    onClick={onBtnClick}
                                    text={"Добавить в корзину"}/>
                            {isModalOpen &&
                            <Modal header={"Добавить товар в корзину?"}
                                   text={"Вы точно хотите добавть этот товар в корзину?"}
                                   isModalOpen={true}
                                   closeButton={true}
                                   onClick={closeModals}
                                   backgroundColor="black"
                                   action={
                                       <>
                                           <Button buttonClass="btn" backgroundColor="#black"
                                                   onClick={closeModal}

                                                   text="Да"/>
                                           <Button buttonClass="btn" backgroundColor="#black"
                                                   onClick={closeModals}
                                                   text="Нет"/>
                                       </>
                                   }/>
                            }
                        </div>
                    </div>
                </Route>

                <Route path="/basket" exact>
                    <div className="products">
                        <div className="jo">
                            <img className="Avatar" src={picture} alt='snowboard'/>
                            <h5>{name}</h5>
                            <div><b>Цена </b>{price}</div>
                            <div><b>Код товара </b>{code}</div>
                            <div><b>Цвет </b>{colour}</div>
                            <Button buttonClass="button" backgroundColor={"black"}
                                    onClick={onBtnClick}
                                    text={"Х"}/>
                            {isModalOpen &&
                            <Modal header={"Удальть товар с корзины?"}
                                   text={"Вы точно хотите удалить этот товар с корзины?"}
                                   isModalOpen={true}
                                   closeButton={true}
                                   onClick={closeModals}
                                   backgroundColor="black"
                                   action={
                                       <>
                                           <Button buttonClass="btn" backgroundColor="#black"
                                                   onClick={deleteById}
                                                   text="Да"/>

                                           <Button buttonClass="btn" backgroundColor="#black"
                                                   onClick={closeModals}
                                                   text="Нет"/>
                                       </>
                                   }/>
                            }
                        </div>
                    </div>
                </Route>
                <Route path="/Home" exact>
                    <div className="products">
                        <div className="jo">
                            <div className="parallelogram">
                                <div onClick={closeFavorite}>
                                    {star(starShowFilled)}
                                </div>
                            </div>
                            <img className="Avatar" src={picture} alt='snowboard'/>
                            <h5>{name}</h5>
                            <div><b>Цена </b>{price}</div>
                            <div><b>Код товара </b>{code}</div>
                            <div><b>Цвет </b>{colour}</div>

                            <Button buttonClass="button" backgroundColor={"black"}
                                    onClick={onBtnClick}
                                    text={"Добавить в корзину"}/>

                            {isModalOpen &&

                            <Modal header={"Добавить товар в корзину?"}
                                   text={"Вы точно хотите добавть этот товар в корзину?"}
                                   isModalOpen={true}
                                   closeButton={true}
                                   onClick={closeModals}
                                   backgroundColor="black"
                                   action={
                                       <>
                                           <Button buttonClass="btn" backgroundColor="#black"
                                                   onClick={closeModal}
                                                   text="Да"/>
                                           <Button buttonClass="btn" backgroundColor="#black"
                                                   onClick={closeModals}
                                                   text="Нет"/>
                                       </>
                                   }/>
                            }
                        </div>
                    </div>
                </Route>

                <Route path="/favorites" exact>
                    <div className="products">
                        <div className="jo">
                            <div className="parallelogram">
                                <div onClick={closeFavorit}>
                                    {star(true)}
                                </div>
                            </div>
                            <img className="Avatar" src={picture} alt='snowboard'/>
                            <h5>{name}</h5>
                            <div><b>Цена </b>{price}</div>
                            <div><b>Код товара </b>{code}</div>
                            <div><b>Цвет </b>{colour}</div>
                        </div>
                    </div>
                </Route>
            </Switch>


        </>

    );
};


Product.propTypes = {
    card: PropTypes.shape({
        name: PropTypes.string,
        price: PropTypes.string,
        src: PropTypes.string,
        article: PropTypes.string,
        color: PropTypes.string,
        onClick: PropTypes.func
    })
};


export default Product;


